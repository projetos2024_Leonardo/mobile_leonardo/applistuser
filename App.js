import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import Details from './src/details';
import ListUsers from './src/list';


const Stack = createStackNavigator();



export default function App() {
  return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName='Lista'>
            <Stack.Screen name="Lista" component={ListUsers}/>
            <Stack.Screen name="Detalhes" component={Details} options={{title:'Detalhes'}}/>

        </Stack.Navigator>
      </NavigationContainer>
  );




  
}