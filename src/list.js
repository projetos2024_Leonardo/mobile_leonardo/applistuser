import React, { useEffect, useState } from "react";
import { View, Text, FlatList, TouchableOpacity } from "react-native";
import axios from "axios";

const ListUsers = ({ navigation }) => {
  const [users, setUsers] = useState([]);
  useEffect(() => {
    getUsers();
  }, []);

  async function getUsers() {
    try {
      const response = await axios.get(
        "https://jsonplaceholder.typicode.com/users"
      );
      setUsers(response.data);
    } catch (error) {
      
    }
  }

  const taskPress = (user) => {
    navigation.navigate("Detalhes", { user });
  };

  return (
    <View>
      <FlatList
        data={users} 
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => (
          <TouchableOpacity onPress={() => taskPress(item)}>
            <Text> {item.name}</Text> 
          </TouchableOpacity>
        )}
      />
    </View>
  );
};

export default ListUsers;
